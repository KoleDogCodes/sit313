import { createContext } from "react";

export const PostTaskContext  = createContext(null);
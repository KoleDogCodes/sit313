const mongoose = require('mongoose');

module.exports = mongoose.model('Experts', new mongoose.Schema({

    expertId: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                var result = false;
                
                if (v.split('@').length == 2){
                    if (v.split('@')[1].split('.').length >= 2) {
                        result = true;
                    }
                } 

                return result;
            },
            message: props => `${props.value} is not a email`
        }
    },

    password: {
        type: String,
        required: true
    },

    address: {
        type: String,
        required: true
    },

    mobile: Number
}));
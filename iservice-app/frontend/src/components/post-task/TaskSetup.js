import React, { useContext } from 'react'
import { Container, Divider, Form, Grid, Header, Icon, Label } from 'semantic-ui-react'
import { PostTaskContext } from '../../contexts/PostTaskContext';

export default function TaskSetup({ loading }) {
    const { taskContextData, setTaskContextData } = useContext(PostTaskContext);

    const handleSuburbChange = (e, data) => {
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors, { taskSuburb: data.value.length < 3 });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskSuburb: data.value,
            formErrors: updatedFormErrors
        });

        setTaskContextData(updatedTaskContextData);
    };

    const handleDateChange = (e, data) => {
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors, { taskDate: data.value.length < 5 });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskDate: data.value,
            formErrors: updatedFormErrors
        });

        setTaskContextData(updatedTaskContextData);
    };

    return (
        <Container fluid>
            <Divider horizontal className='divider-header'>
                <Header as='h4'>
                    <Icon name='setting' />
                    Setting up your task
                </Header>
            </Divider>

            <Form>
                <Grid columns={2}>
                    {taskContextData.taskType === 0 ?
                        <Grid.Row >
                            <Grid.Column width={3}>
                                <Label>Enter a suburb:</Label>
                            </Grid.Column>
                            <Grid.Column width={13}>
                                <Form.Input placeholder='Enter suburb...' className='fluid-input' onChange={handleSuburbChange} disabled={loading} error={taskContextData.formErrors.taskSuburb} />
                            </Grid.Column>
                        </Grid.Row>
                        :
                        ''
                    }

                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Label>Enter a date:</Label>
                        </Grid.Column>
                        <Grid.Column width={13}>
                            <Form.Input placeholder='Enter date...' type='date' className='fluid-input' onChange={handleDateChange} disabled={loading} error={taskContextData.formErrors.taskDate} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Form>
        </Container>
    )
}

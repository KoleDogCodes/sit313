const mongoose = require('mongoose');

module.exports = mongoose.model('Messages', new mongoose.Schema({
    id: {
        type: String
    },
    
    taskId: {
        type: String
    },

    authorId: {
        type: String
    },

    message: {
        type: String
    },

    date: {
        type: String
    }
}));
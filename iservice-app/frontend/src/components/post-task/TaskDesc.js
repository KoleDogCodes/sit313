import React, { useContext } from 'react'
import { Container, Divider, Form, Grid, Header, Icon, Label } from 'semantic-ui-react'
import { PostTaskContext } from '../../contexts/PostTaskContext';

export default function TaskDesc({ loading }) {
    const { taskContextData, setTaskContextData } = useContext(PostTaskContext);

    const handleTitleChange = (e, data) => {
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors, { taskTitle: data.value.length < 3 });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskTitle: data.value, 
            formErrors: updatedFormErrors
        });

        setTaskContextData(updatedTaskContextData);
    };

    const handleDescChange = (e, data) => {
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors,  {taskDesc: data.value.length < 10 });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskDesc: data.value.trim(), 
            formErrors: updatedFormErrors
        });

        setTaskContextData(updatedTaskContextData);
    };

    return (
        <Container fluid>
            <Divider horizontal className='divider-header'>
                <Header as='h4'>
                    <Icon name='pencil' />
                    Describe your task
                </Header>
            </Divider>

            <Form>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Label>Enter task title:</Label>
                        </Grid.Column>
                        <Grid.Column width={13}>
                            <Form.Input placeholder='Enter task title...' className='fluid-input' onChange={handleTitleChange} disabled={loading} error={taskContextData.formErrors.taskTitle} />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Label>Enter task desc:</Label>
                        </Grid.Column>
                        <Grid.Column width={13}>
                            <Form.TextArea placeholder='Enter task description...' className='fluid-input' onChange={handleDescChange} disabled={loading} error={taskContextData.formErrors.taskDesc} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Form>
        </Container>
    )
}

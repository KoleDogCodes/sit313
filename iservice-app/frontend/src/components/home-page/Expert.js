import React from 'react'
import { Card, Image, Rating } from 'semantic-ui-react'

export default function Expert({ src, name, desc, rating }) {
    return (
        <Card raised>
            <Card.Content>
                <Image
                    size='small'
                    src={src}
                    style={{paddingBottom: '5px', width: '100%'}}
                />
                <Card.Header>{name}</Card.Header>
                <Card.Description>{desc}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Rating icon='star' maxRating={5} rating={rating} disabled/>
            </Card.Content>
        </Card>
    )
}

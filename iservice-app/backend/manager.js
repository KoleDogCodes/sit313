const Users = require('./schemas/user');
const Experts = require('./schemas/expert');
const Messages = require('./schemas/message');
const Tasks = require('./schemas/task');
const crypto = require('crypto');
const mailer = require('@sendgrid/mail');
const fs = require("fs");
const redis = require('async-redis');

//Variables
const salt = 'nHXVpT4ux2';
const redisClient = redis.createClient({ host: process.env.REDIS_HOST, port: process.env.REDIS_PORT });

redisClient.on('error', error => {
    console.log('[Redis Error]', error);
});

module.exports = {
    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    setEmailDetails: function (key) {
        mailer.setApiKey(key);
    },

    sendEmail: function (to, subject, text) {
        const msg = {
            to,
            from: 'electrotech14@hotmail.com', // Use the email address or domain you verified above
            subject,
            text
        };

        mailer.send(msg).then(() => { }, error => {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        });
    },

    registerAccount: async function (country, firstName, lastName, email, password, address, city, state, zip, mobile) {
        var errors = [];
        email = email.toLowerCase();

        const newUser = new Users({
            customerId: this.hash(email, firstName + lastName),
            country: country,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: this.hash(email, password),
            address: address,
            city: city,
            state: state,
            role: 'customer',
            zip: zip,
            mobile: mobile,
            avatar: 'https://react.semantic-ui.com/images/avatar/large/matthew.png'
        });

        var error = newUser.validateSync();
        console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        var promise = newUser.save(err => { });
        await promise;

        if (errors.length == 0) {
            this.sendEmail(email,
                "IServiceDB Welcome",
                `Hi ${firstName},
                Welcome to the IServiceDB platform!
                We perform many things like 3d printing, pcb making and much more!

                Thanks,
                IServiceDB Team
                `);
        }

        return errors;
    },

    registerGoogleAccount: async function (id, email, name, avatar) {
        var errors = [];
        email = email.toLowerCase();
        const hashedId = this.hash(id, salt);

        const newUser = new Users({
            customerId: hashedId,
            country: ' ',
            firstName: name,
            lastName: ' ',
            email: email,
            password: ' ',
            address: ' ',
            city: ' ',
            state: ' ',
            role: 'customer',
            zip: -1,
            mobile: -1,
            avatar: avatar
        });

        var error = newUser.validateSync();
        console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        var promise = newUser.save(err => { });
        await promise;

        if (errors.length == 0) {
            this.sendEmail(email,
                "IServiceDB Welcome",
                `Hi ${name},
                Welcome to the IServiceDB platform!
                We perform many things like 3d printing, pcb making and much more!

                Thanks,
                IServiceDB Team
                `);

            return { success: true, data: { id: hashedId, role: 'customer', avatar: avatar, name: name } };
        }

        return { success: false, errors: errors };
    },

    updateCustomerData: async function (id, firstName, lastName, avatar, role) {
        if (avatar != undefined && !avatar.startsWith('data:image/')) {
            return { success: false, message: 'invalid-image' };
        }

        const ext = avatar == undefined ? '' : avatar.split(',')[0].replace('data:image/', '').replace(';base64', '');
        var promise = Users.findOne({ customerId: id }, (err, document) => {
            if (document != undefined) {
                document.firstName = firstName || document.firstName;
                document.lastName = lastName || document.lastName;
                document.role = role || document.role;
                document.avatar = avatar != undefined ? `${id}.${ext}` : document.avatar;

                document.save(err => { });
            }
        }).catch(err => { });

        if (avatar != undefined) {
            //Save image on disk
            if (!fs.existsSync('./public/profile')) fs.mkdirSync('./public/profile')
            fs.writeFile(`./public/profile/${id}.${ext}`, avatar.split(',')[1], 'base64', function (err) {
                if (err) {
                    console.log(err);
                    result = false;
                }
            });
        }

        const promiseValue = await promise;
        // var cachedValue = await this.getCustomerDataById(id);
        // cachedValue.role = role != undefined ? role : cachedValue.role;
        // console.log(cachedValue);
        await redisClient.del(id);

        return { success: true };
    },

    updateExternalCustomerData: async function (id, name, email, avatar) {
        const hashedId = this.hash(id, salt);
        const accountExists = await this.customerIdExists(hashedId);

        if (!accountExists) {
            return await this.registerGoogleAccount(id, email, name, avatar);
        }

        const fetchedData = await this.getCustomerDataById(hashedId);
        return { success: true, data: fetchedData };
    },

    verifyLoginDetails: async function (email, password) {
        var promise = Users.findOne({ email: email.toLowerCase() });
        const promiseValue = await promise;
        const hashedPassword = this.hash(email, password);
        const detailsIsCorrect = promiseValue == undefined ? false : (hashedPassword == promiseValue.password);

        return detailsIsCorrect ? (await this.getCustomerData(email)) : undefined;
    },

    customerIdExists: async function (key) {
        var promise = Users.findOne({ customerId: key });
        const promiseValue = await promise;
        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    customerEmailExists: async function (email) {
        var promise = Users.findOne({ email: email.toLowerCase() });
        const promiseValue = await promise;
        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getCustomerDataById: async function (id) {
        if (await redisClient.exists(id)) {
            return JSON.parse(await redisClient.get(id));
        }

        var promise = Users.findOne({ customerId: id });
        const promiseValue = await promise;
        const cleanedData = {
            id: promiseValue.customerId,
            role: promiseValue.role || 'customer',
            name: `${promiseValue.firstName} ${promiseValue.lastName}`,
            country: promiseValue.country,
            avatar: promiseValue.avatar
        };

        await redisClient.set(id, JSON.stringify(cleanedData));

        return promiseValue == undefined ? {} : cleanedData;
    },

    getCustomerData: async function (email) {
        var promise = Users.findOne({ email: email.toLowerCase() });
        const promiseValue = await promise;

        return {
            id: promiseValue.customerId,
            role: promiseValue.role || 'customer',
            avatar: promiseValue.avatar,
            name: `${promiseValue.firstName} ${promiseValue.lastName}`
        };
    },

    accountExists: async function (email) {

        var promise = Users.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    /////// EXPERTS ///////

    expertIdExists: async function (id) {

        var promise = Experts.findOne({ expertId: id });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getExpertData: async function (expertId) {
        var promise = Experts.findOne({ expertId: expertId });

        const promiseValue = await promise;

        return promiseValue || {};
    },

    addExpert: async function (email, password, address, mobile) {
        var errors = [];
        email = email.toLowerCase();
        const id = this.hash(Date.now().toString(), Date.now().toString());

        const newExpert = new Experts({
            expertId: id,
            email: email,
            password: this.hash(email, password),
            address: address,
            mobile: mobile
        });

        var error = newExpert.validateSync();
        console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        await newExpert.save(err => { });

        return { errors: errors, id: id };
    },

    updateExpertData: async function (expertId, newEmail, newPassword, newAddress, newMobile) {
        var result = false;

        var promise = Experts.findOne({ expertId: expertId }, (err, document) => {
            if (document != undefined) {
                const newHashedPassword = this.hash(newEmail || document.email, newPassword || document.password);
                document.email = newEmail || document.email;
                document.password = newHashedPassword || document.password;
                document.address = newAddress || document.address;
                document.mobile = newMobile || document.mobile;

                result = true;

                document.save(err => { });
            }
        });

        const promiseValue = await promise;

        return result == true ? { success: true } : { success: false };
    },

    deleteExpertData: async function (expertId) {
        var promise = Experts.deleteOne({ expertId: expertId });

        const promiseValue = await promise;

        return promiseValue || {};
    },

    /////// TASKS ///////

    taskIdExists: async function (id) {
        var promise = Tasks.findOne({ id: id });
        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getTaskData: async function (taskId) {
        var promise = Tasks.findOne({ id: taskId });
        const promiseValue = await promise;

        return promiseValue || {};
    },

    getTasksData: async function () {
        var promise = Tasks.find({}, '-_id');
        const promiseValue = await promise;

        return promiseValue || {};
    },


    getMessagesData: async function (taskId) {
        var promise = (await Messages.find({ taskId }).sort({ _id: -1 }).limit(50)).reverse();
        var promiseValue = await promise;
        return promiseValue || {};
    },

    addTask: async function (taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget, taskImage) {
        const id = this.hash(Date.now().toString(), Date.now().toString());
        var errors = [];

        const newTask = new Tasks({
            id: id,
            type: taskType,
            title: taskTitle,
            desc: taskDesc,
            image: taskImage,
            suburb: taskSuburb,
            date: taskDate,
            budgetType: budgetType,
            budget: taskBudget
        });

        var error = newTask.validateSync();
        //console.log(error);
        if (error != undefined) {
            for (const [key, value] of Object.entries(error.errors)) {
                errors.push(key);
            }
        }

        await newTask.save(err => { });

        return { errors: errors, id: id };
    },

    updateTaskData: async function (taskId, taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget) {
        var result = false;

        var promise = Tasks.findOne({ id: taskId }, async (err, document) => {
            if (document != undefined) {
                document.type = taskType || document.type;
                document.title = taskTitle || document.title;
                document.desc = taskDesc || document.desc;
                document.suburb = taskSuburb || document.suburb;
                document.date = taskDate || document.date;
                document.budgetType = budgetType || document.budgetType;
                document.budget = taskBudget || document.budget;
                result = true;

                await document.save(err => { });
            }
        });

        const promiseValue = await promise;
        console.log(promiseValue);
        return result ? { success: true } : { success: false };
    },

    deleteTaskData: async function (taskId) {
        var promise = Tasks.deleteOne({ id: taskId });
        var promiseMessages = Messages.deleteMany({ taskId: taskId });

        const promiseValue = await promise;
        const promiseValueMessages = await promiseMessages;

        return promiseValue || {};
    },
}
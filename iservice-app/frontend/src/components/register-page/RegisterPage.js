import React, { useState } from 'react'
import { Container, Form, Grid, Image, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import { getApiEndpoint, sendRequest } from '../../utility/RequestUtility'

const formFields = ['country', 'firstName', 'lastName', 'email', 'password', 'confirmPassword', 'address', 'city', 'state'];
const countryOptions = [
    { key: '0', text: 'Australia', value: 'Australia' },
    { key: '1', text: 'United States', value: 'United States' },
    { key: '2', text: 'United Kingdom', value: 'United Kingdom' },
];

export default function RegisterPage() {
    const [loading, setLoadingState] = useState(false);
    const [accountData, setAccountData] = useState({country: countryOptions[0].value});

    const handleClick = async (e, data) => {
        setLoadingState(true);
        console.log(accountData);
        
        //Veirfy all fields were filled
        var foundAll = true;
        formFields.forEach(name => {
            if (!(name in accountData)) foundAll = false;
        });

        if (!foundAll) {
            setLoadingState(false);
            return;
        }

        //Send post request and wait for response
        const response = (await sendRequest(`${getApiEndpoint()}/account/register`, "POST", accountData)).response.errors;
        
        if (response.length > 0) {
            Swal.fire('Whoops!', `Looks like we had the following issues [${response}] when creating your account?`, 'error');
            setLoadingState(false);
            return;
        }
        
        Swal.fire('Welcome!', `Looks like we could create your account! \nPlease head to the login page`, 'success');
        setLoadingState(false);
    };

    const handleChange = (e, data) => {
        const updatedAccountData = Object.assign({}, accountData, {[data.name]: data.value});
        setAccountData(updatedAccountData);
    };

    return (
        <Container>
            <Grid textAlign='center' style={{marginTop: '5px'}}>
                <Grid.Row>
                    <Grid.Column textAlign='left'>
                        <Segment raised>
                            <Image src='/logo.png' size='small' centered />
                            <Form loading={loading}>
                                <Form.Select fluid name='country' label='Country of Residence' placeholder='Select country' required options={countryOptions} defaultValue={countryOptions[0].value} onChange={handleChange}/>
                                
                                <Form.Group widths='equal'>
                                    <Form.Input name='firstName' fluid label='First name' placeholder='First name' required onChange={handleChange}/>
                                    <Form.Input name='lastName' fluid label='Last name' placeholder='Last name' required onChange={handleChange}/>
                                </Form.Group>

                                <Form.Input name='email' fluid label='Email' placeholder='Email' type='email' required onChange={handleChange}/>
                                <Form.Input name='password' fluid label='Password' placeholder='Password' type='password' required onChange={handleChange}/>
                                <Form.Input name='confirmPassword' fluid label='Confirm Password' placeholder='Password again' type='password' required onChange={handleChange}/>
                                <Form.Input name='address' fluid label='Address' placeholder='Address' required onChange={handleChange}/>

                                <Form.Group widths='equal'>
                                    <Form.Input name='city' fluid label='City' placeholder='City' required onChange={handleChange}/>
                                    <Form.Input name='state' fluid label='State, Province or Region' placeholder='State' required onChange={handleChange}/>
                                </Form.Group>

                                <Form.Button fluid onClick={handleClick}>Register</Form.Button>
                            </Form>
                        </Segment>

                        <div>
                            Already have an account? <Link to='/login'>Sign in here</Link>
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

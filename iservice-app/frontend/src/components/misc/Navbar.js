import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Dropdown, Icon, Image, Menu } from 'semantic-ui-react'
import NavBarAvatar from './NavBarAvatar'
import { fetchCachedUser, clearCachedUser } from '../../utility/RequestUtility'

export default function Navbar({ authed, setAuthState }) {
    //Active Item State
    const [activeItem, setActiveItem] = useState('');

    //Menu Item Click
    const onClick = (event, data) => {
        setActiveItem(data.name);
    };

    const handleSignOutClick = (e, data) => {
        clearCachedUser();
        setAuthState(false);
    };

    return (
        <>
            <Menu pointing fixed='top' className='menubar' inverted borderless>
                <Menu.Item name='logo' header as='div' onClick={onClick}>
                    <Link to="/" className='nav-link'>
                        <Image src='/logo.png' size='mini' centered style={{ width: '24px', height: '24px' }} />
                    </Link>
                </Menu.Item>

                {!authed ?
                    <Menu.Item name='sign-in' active={activeItem === 'sign-in'} position='right' as='div' onClick={onClick}>
                        <Link to="/login"><Icon name='sign-in' />Sign In</Link>
                    </Menu.Item>
                    :
                    <Menu.Menu position='right'>
                        <Dropdown item trigger={<NavBarAvatar source={authed ? fetchCachedUser().avatar || `https://picsum.photos/1080` : undefined} name={authed ? fetchCachedUser().name : 'Guest'} />}>
                            <Dropdown.Menu>
                                <Link to="/expert/creation" className='nav-link'>
                                    <Dropdown.Item className='nav-link'><Icon name='handshake' />
                                        {authed && fetchCachedUser().role === 'expert' ? 'Revoke expert role' : 'Become an expert'}
                                    </Dropdown.Item>
                                </Link>

                                <Link to="/task/post" className='nav-link'>
                                    <Dropdown.Item className='nav-link'><Icon name='upload' />Post a task</Dropdown.Item>
                                </Link>

                                <Link to="/task/find" className='nav-link'>
                                    <Dropdown.Item className='nav-link'><Icon name='search' />Find tasks</Dropdown.Item>
                                </Link>

                                <Link to="/donate" className='nav-link'>
                                    <Dropdown.Item className='nav-link'><Icon name='money' />Donate</Dropdown.Item>
                                </Link>

                                <Dropdown.Divider />
                                <Link to="/profile/edit" className='nav-link'>
                                    <Dropdown.Item className='nav-link'><Icon name='user' />Profile</Dropdown.Item>
                                </Link>

                                <Link to='/login' className='nav-link'>
                                    <Dropdown.Item className='nav-link' onClick={handleSignOutClick}><Icon name='sign out' />Sign Out</Dropdown.Item>
                                </Link>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Menu>
                }
            </Menu>
        </>
    )
}

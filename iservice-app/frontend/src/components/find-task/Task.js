import React from 'react'
import { Card, Grid, Icon, Image, Popup } from 'semantic-ui-react'
import Swal from 'sweetalert2'
import { getApiEndpoint, sendRequest, fetchCachedUser } from '../../utility/RequestUtility';
import TaskExpanded from './TaskExpanded'
import { Link } from 'react-router-dom'

const MAX_DESC_LENGTH = 350;

function timedSwalAlert(title, text, icon, timer) {
    let timerInterval
    Swal.fire({
        title: title,
        text: text,
        timer: timer,
        icon: icon,
        willClose: () => {
            clearInterval(timerInterval);
        }
    });
}

export default function Task({ id, src, type, title, desc, suburb, date, budget, budgetType, onDelete }) {
    const handleDeleteClick = (e, data) => {
        if (fetchCachedUser().role !== 'expert') {
            Swal.fire('Oh dear!', 'Looks like you do not have permission to delete tasks', 'error');
            return;
        }

        Swal.fire({
            title: title,
            text: 'Are you sure you want to delete this task?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            backdrop: true,
            preConfirm: (request) => {
                return sendRequest(`${getApiEndpoint()}/tasks/${id}`, 'DELETE', { userId: fetchCachedUser().id });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if ((result.value === undefined || !result.value.response.success) && result.isConfirmed) {
                Swal.fire('Oh dear!', 'Seems like we had some issues trying to delete this task', 'error');
            }
            else if (result.isConfirmed) {
                //Swal.fire('Poof!', 'Task successfully deleted', 'success');
                timedSwalAlert('Poof!', 'Task successfully deleted', 'success', 1000);
                onDelete(id);
            }
        });
    };

    return (
        <Card raised>
            <Image src={src} fluid wrapped label={{
                as: 'a',
                color: 'black',
                content: budgetType === 1 ? `${budget}/h` : budget,
                icon: 'dollar',
                ribbon: true,
            }} />
            <Card.Content>
                <Card.Header style={{ wordWrap: 'break-word' }}>{title}</Card.Header>
                <Card.Meta>
                    <span className='date'>Issued at: {date}</span>
                </Card.Meta>
                <Card.Description style={{ wordWrap: 'break-word' }}>
                    {desc.length > MAX_DESC_LENGTH ? desc.substring(0, MAX_DESC_LENGTH) + '...' : desc}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            <TaskExpanded
                                id={id}
                                type={type}
                                src={src}
                                title={title}
                                desc={desc}
                                suburb={suburb}
                                date={date}
                                budget={budget}
                                budgetType={budgetType}
                                triggerModal={<Icon name='expand' className='clickable-icon' fitted> See More...</Icon>}
                            />
                        </Grid.Column>
                        <Grid.Column width={6} textAlign='right'>
                            <Popup content='Chat Window' trigger={<Link to={`/chat/${id}`}> <Icon name='chat' fitted color='blue' className='clickable-icon'>&nbsp;&nbsp;</Icon> </Link>} />
                            <Popup content='Note: This will delete the task!' trigger={<Icon name='trash' fitted color='red' className='clickable-icon' onClick={handleDeleteClick} />} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Card.Content>
        </Card>
    )
}

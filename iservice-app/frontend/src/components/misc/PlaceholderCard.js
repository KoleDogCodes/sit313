import React from 'react'
import { Grid, Placeholder, Segment } from 'semantic-ui-react';

function generatePlaceHolders(count) {
    var result = [];

    for (let index = 0; index < count; index++) {
        result.push(
            <Grid.Column key={index}>
                <Segment raised>
                    <Placeholder>
                        <Placeholder.Header image>
                            <Placeholder.Line />
                            <Placeholder.Line />
                        </Placeholder.Header>
                        <Placeholder.Paragraph>
                            <Placeholder.Line length='medium' />
                            <Placeholder.Line length='short' />
                        </Placeholder.Paragraph>
                    </Placeholder>
                </Segment>
            </Grid.Column>);
    }

    return result;
}

export default function PlaceholderCard({ columns, rows, display }) {
    return (
        <Grid columns={columns} stackable>
            {display ? generatePlaceHolders(columns * rows) : <Grid.Column></Grid.Column>}
        </Grid>
    )
}

import React, { useState } from 'react'
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js'
import { Button, Container, Divider, Form, Grid, Header, Icon, Message, Segment } from 'semantic-ui-react'
import { getApiEndpoint, sendRequest } from '../../utility/RequestUtility'
import { Link } from 'react-router-dom';

const CARD_OPTIONS = {
    iconStyle: "solid",
    style: {
        base: {
            iconColor: "#c4f0ff",
            color: "black",
            fontSize: "16px",
            fontSmoothing: "antialiased",
            ":-webkit-autofill": { color: "#fce883" },
            "::placeholder": { color: "#87bbfd" }
        },
        invalid: {
            color: "red"
        }
    }
}

export default function Donate() {
    const stripe = useStripe();
    const elements = useElements();
    const [donationSuccess, setDonationSuccess] = useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [amount, setAmount] = useState(0);

    const handleNameChange = (e, data) => setName(data.value);
    const handleEmailChange = (e, data) => setEmail(data.value);
    const handleAmountChange = (e, data) => setAmount(parseFloat(data.value));

    const handleSubmit = async (event) => {
        event.preventDefault();

        //Check if stripe has loaded
        if (!stripe || !elements) {
            return;
        }

        const cardElement = elements.getElement(CardElement);

        // Use your card Element with other Stripe.js APIs
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: cardElement,
        });

        if (!error) {
            const { id } = paymentMethod;

            const response = (await sendRequest(`${getApiEndpoint()}/charge`, 'POST', {
                stripeToken: id,
                amount: amount,
                name: name,
                email: email
            })).response;

            console.log(response);
            setDonationSuccess(response.success);
        }
    };

    return (
        <Container>
            <Grid verticalAlign='middle' style={{ height: '90vh' }}>
                <Grid.Row>
                    <Grid.Column>
                        <Message positive hidden={!donationSuccess}>
                            <Message.Header>Donation Successful</Message.Header>
                            <p>Thank you for your donation! Head back to the homepage <Link to='/'>here.</Link></p>
                        </Message>

                        <Divider horizontal className='divider-header'>
                            <Header as='h4'>
                                <Icon name='money' />
                                Donation
                            </Header>
                        </Divider>

                        <Segment color='purple'>
                            <Form action={`${getApiEndpoint()}/charge`} method='POST' onSubmit={handleSubmit}>
                                <Form.Field>
                                    <Form.Input placeholder='Enter full name' label='Full Name' type='text' required onChange={handleNameChange}/>
                                </Form.Field>
                                <Form.Field>
                                    <Form.Input placeholder='Enter email' label='Email Address' type='email' required onChange={handleEmailChange}/>
                                </Form.Field>
                                <Form.Field>
                                    <Form.Input placeholder='Enter amount' label='Amount' type='number' required onChange={handleAmountChange}/>
                                </Form.Field>
                                <Form.Field>
                                    <CardElement options={CARD_OPTIONS} />
                                </Form.Field>
                                <Form.Field>
                                    <Button type='submit' disabled={!stripe} color='blue' content='Donate' fluid />
                                </Form.Field>
                            </Form>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

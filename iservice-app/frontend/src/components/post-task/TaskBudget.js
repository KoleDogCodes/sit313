import React, { useContext, useState } from 'react'
import { Button, Container, Divider, Form, Grid, Header, Icon, Label, Radio } from 'semantic-ui-react'
import { PostTaskContext } from '../../contexts/PostTaskContext';

export default function TaskBudget({ onSubmit, loading }) {
    const [taskBudgetType, setTaskBudgetType] = useState(0);
    const { taskContextData, setTaskContextData } = useContext(PostTaskContext);

    const handleChange = (e, data) => {
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            budgetType: parseInt(data.value)
        });
        setTaskBudgetType(parseInt(data.value));
        setTaskContextData(updatedTaskContextData);
    }

    const handleBudgetInputChange = (e, data) => {
        var updatedFormErrors = Object.assign({}, taskContextData.formErrors, { taskBudget: parseFloat(data.value) <= 0 });
        var updatedTaskContextData = Object.assign({}, taskContextData, {
            taskBudget: parseFloat(data.value),
            formErrors: updatedFormErrors
        });

        setTaskContextData(updatedTaskContextData);
    };

    return (
        <Container fluid>
            <Divider horizontal className='divider-header'>
                <Header as='h4'>
                    <Icon name='question circle' />
                    Suggest how much
                </Header>
            </Divider>

            <Grid columns={2}>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Label className='lbl'> Select budget type: </Label>
                        <Radio
                            label='Total'
                            name='taskBudgetTotal'
                            value='0'
                            checked={taskBudgetType === 0}
                            onChange={handleChange}
                            disabled={loading}
                        />

                        <Radio style={{ marginLeft: '10px' }}
                            label='Hourly Rate'
                            name='taskBudgetHourly'
                            value='1'
                            checked={taskBudgetType === 1}
                            onChange={handleChange}
                            disabled={loading}
                        />
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={8}>
                        <Form.Input icon='dollar' iconPosition='left' placeholder='Enter your budget...' type='number' className='fluid-input' disabled={loading} onChange={handleBudgetInputChange} error={true}/>
                    </Grid.Column>
                    <Grid.Column width={8} textAlign='right'>
                        <Button content='POST TASK' onClick={onSubmit} loading={loading} disabled={loading}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

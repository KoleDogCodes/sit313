const express = require('express');
const mongoose = require('mongoose');
const manager = require('./manager');
const app = express();
const redis = require('async-redis');
const stripe = require('stripe')('sk_test_51JZuQRI0HeNEJSyCD3BTPWNC85sT8NdynAbeAaJgCKy0QrjoEI2P5qBpRACEE8RUxdaSGCAOFeprHChqL0NnyYGC00mBCJGLXP');

//Static Variables
const port = process.env.PORT || 3000;
const base = `${__dirname}/public`;

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});

//Setting express to encode reques
app.use(express.json({ extended: true, limit: '2mb' }));

//Setup public folder
app.use(express.static('public'));

//Connect to the database
mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true });
manager.setEmailDetails(process.env.SENDGRID_API_KEY);

app.get('/', (req, res) => {
    return res.json({ success: true, message: "REST api server is up!" });
});

/// PAYMENT ///
app.post("/api/charge", async (req, res) => {
    const { amount, stripeToken } = req.body

    try {
        const payment = await stripe.paymentIntents.create({
            amount,
            currency: "AUD",
            description: "IService Donation",
            payment_method: stripeToken,
            confirm: true
        });

        console.log("Payment", payment);

        res.json({
            message: "Payment successful",
            success: true
        });
    }
    catch (error) {
        console.log("Error", error);

        res.json({
            message: "Payment failed",
            success: false
        });
    }
});

/// ACCOUNTS ///
app.get('/api/account/login', async (req, res) => {
    if (req.query.email == undefined || req.query.password == undefined) {
        return res.json({ success: false, errors: ['invalid-request'] });
    }

    const { email, password } = req.query;
    const result = await manager.verifyLoginDetails(email, password);

    return res.json({ success: result != undefined, data: result, errors: [] });
});

app.get('/api/account/user/:id', async (req, res) => {
    const id = req.params.id;
    const result = await manager.getCustomerDataById(id);
    return res.json({ success: true, data: result });
});

app.post('/api/account/register', async (req, res) => {
    const { country, firstName, lastName, email, password, confirmPassword, address, city, state, zip, mobile } = req.body;
    console.log(req.body);

    //Server-side validation (Passwords)
    if (password != confirmPassword) {
        return res.json({
            success: false,
            errors: ['password']
        });
    }

    const searchResult = await manager.accountExists(email || '');
    if (searchResult) {
        return res.json({
            success: false,
            errors: ['email-exists']
        });
    }

    //Only create account if email does not exist
    if (!searchResult) {
        const creationResult = await manager.registerAccount(country, firstName, lastName, email, password, address, city, state, zip, mobile);

        if (creationResult.length == 0) {
            return res.json({
                success: true,
                message: `Account has been created!`,
                errors: []
            });
        }
    }

    return res.json({
        success: false,
        message: `Failed to create account`,
        errors: creationResult
    });
});

app.put('/api/account/edit', async (req, res) => {
    const { id, firstName, lastName, avatar, role } = req.body;
    const result = await manager.updateCustomerData(id, firstName, lastName, avatar, role);
    return res.json(result);
});

app.put('/api/account/google/edit', async (req, res) => {
    const { id, name, email, avatar } = req.body;
    const result = await manager.updateExternalCustomerData(id, name, email, avatar);
    return res.json(result);
});

/// EXPERTS ///
app.post('/api/experts', async (req, res) => {
    const { email, password, address, mobile } = req.body;
    const result = await manager.addExpert(email, password, address, mobile);

    if (result.errors.length == 0) {
        return res.json({
            errors: [],
            message: 'Expert data has been added',
            id: result.id || "NIL"
        });
    }

    return res.json({
        message: 'The request contained the following errors',
        errors: result.errors
    });
});

app.get('/api/experts/:id', async (req, res) => {
    const id = req.params.id;
    const expertData = await manager.getExpertData(id);
    return res.send(expertData);
});

app.put('/api/experts/:id', async (req, res) => {
    const id = req.params.id;
    const { email, password, address, mobile } = req.body;

    const result = await manager.updateExpertData(id, email, password, address, mobile);

    return res.json(result);
});

app.delete('/api/experts/:id', async (req, res) => {
    const id = req.params.id;

    await manager.deleteExpertData(id);

    return res.json({
        message: `Deleted expert ${id}`
    });
});

/// TASKS ///
app.get('/api/tasks', async (req, res) => {
    const result = await manager.getTasksData();

    return res.json({
        data: result,
        success: true
    });
});

app.post('/api/tasks', async (req, res) => {
    const { taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget, taskImage } = req.body;
    console.log(req.body);
    const result = await manager.addTask(taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget, taskImage);

    if (result.errors.length == 0) {
        return res.json({
            errors: [],
            message: 'Task data has been added',
            id: result.id || "NIL"
        });
    }

    return res.json({
        message: 'The request contained the following errors',
        errors: result.errors
    });
});

app.get('/api/tasks/:id', async (req, res) => {
    const id = req.params.id;
    const expertData = await manager.getTaskData(id);
    return res.send(expertData);
});

app.put('/api/tasks/:id', async (req, res) => {
    const id = req.params.id;
    const { taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget } = req.body;

    const result = await manager.updateTaskData(taskType, taskTitle, taskDesc, taskSuburb, taskDate, budgetType, taskBudget);

    return res.json(result);
});

app.delete('/api/tasks/:id', async (req, res) => {
    const id = req.params.id;
    const { userId } = req.body;
    const userRole = (await manager.getCustomerDataById(userId)).role || 'customer';

    if (userRole !== 'expert') {
        return res.json({
            success: false,
            message: `invalid permissions`
        });
    }

    await manager.deleteTaskData(id);

    return res.json({
        success: true,
        message: `Deleted task ${id}`
    });
});

/// MESSAGES ///
app.get('/api/messages/:id', async (req, res) => {
    const id = req.params.id;
    const messages = await manager.getMessagesData(id);
    return res.send(messages);
});

//Web Listener
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
import React, { useState } from 'react'
import { Container } from 'semantic-ui-react'
import TaskType from './TaskType'
import TaskDesc from './TaskDesc'
import TaskSetup from './TaskSetup'
import TaskBudget from './TaskBudget'
import { PostTaskContext } from '../../contexts/PostTaskContext'
import Swal from 'sweetalert2'
import TaskDescImage from './TaskDescImage'
import { getApiEndpoint, sendRequest } from '../../utility/RequestUtility'

export default function PostTask() {
    const [loading, setLoadingState] = useState(false);
    const [taskContextData, setTaskContextData] = useState({
        taskType: 0, budgetType: 0, taskTitle: '', taskSuburb: 'online', formErrors: {
            taskTitle: true,
            taskDesc: true,
            taskSuburb: true,
            taskDate: true,
            taskBudget: true,
            taskImage: true
        }
    });

    const onSubmit = async (e, data) => {
        const hasErrors = taskContextData.formErrors.taskTitle || taskContextData.formErrors.taskDesc || taskContextData.formErrors.taskSuburb || taskContextData.formErrors.taskDate;

        //Check if there are any form errors and display message to user
        if (hasErrors) {
            Swal.fire('Whoops!', 'Please correct the red fields.', 'error');
            return;
        }

        if (taskContextData.formErrors.taskBudget) {
            Swal.fire('Whoops!', 'Please input a valid budget greater than zero.', 'error');
            return;
        }

        if (taskContextData.formErrors.taskImage) {
            Swal.fire('Whoops!', 'Please upload an image.', 'error');
            return;
        }

        //Disable form
        setLoadingState(true);

        //Send post request and wait for response
        const response = await sendRequest(`${getApiEndpoint()}/tasks`, "POST", taskContextData);

        if (!response.success) {
            Swal.fire('Oh no!', `${response.response}`, 'error');
            setLoadingState(false);
            return;
        }

        setLoadingState(false);
        Swal.fire('Task Created!', response.response.message, 'success');
    }

    return (
        <PostTaskContext.Provider value={{ taskContextData, setTaskContextData }}>
            <Container>
                <TaskType loading={loading} />
                <TaskDesc loading={loading} />
                <TaskDescImage />
                <TaskSetup loading={loading} />
                <TaskBudget onSubmit={onSubmit} loading={loading} />
            </Container>
        </PostTaskContext.Provider>
    )
}

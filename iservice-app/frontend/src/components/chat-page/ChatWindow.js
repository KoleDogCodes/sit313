import React, { useEffect, useRef } from 'react'
import {  Comment, Card, Ref } from 'semantic-ui-react'
import ChatMessage from './ChatMessage'

export default function ChatWindow({ messages }) {
    const messagesEndRef = useRef(null);

    const scrollToBottom = () => {
        if (messagesEndRef.current !== null) {
            messagesEndRef.current.scroll(0, messagesEndRef.current.scrollHeight);
        }
    }

    //Auto Scroll to bottom
    useEffect(() => { scrollToBottom() }, [messages]);

    return (
        <Ref innerRef={messagesEndRef}>
            <Card.Content style={{ height: 'inherit', overflowY: 'scroll' }} className='chat-window'>
                <Comment.Group>
                    {messages.map((message, index) => (
                        <ChatMessage key={index}
                            author={message.authorName}
                            date={message.date}
                            message={message.message}
                            avatar={message.avatar}
                        />
                    ))}
                </Comment.Group>
            </Card.Content>
        </Ref>
    )
}

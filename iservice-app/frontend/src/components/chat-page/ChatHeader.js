import React, { useEffect, useState } from 'react'
import { Header, Icon } from 'semantic-ui-react'
import { fetchRequest, getApiEndpoint } from '../../utility/RequestUtility';

export default function ChatHeader({ id }) {
    const [title, setTitle] = useState('');

    useEffect(() => {
        fetchRequest(`${getApiEndpoint()}/tasks/${id}`).then(result => {
            const response = result.response;
            setTitle(response.title);
        });
    });

    return (
        <Header textAlign='center' size='small' attached>
            <Icon name='comment alternate' />
            {`${title} Chat Window`}
        </Header>
    )
}

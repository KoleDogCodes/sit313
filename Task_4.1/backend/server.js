const express = require('express');
const mongoose = require('mongoose');
const manager = require('./manager');
const app = express();

//Static Variables
const port = process.env.PORT || 3000;

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});

//Setting express to encode reques
app.use(express.urlencoded({ extended: true }));

//Connect to the database
mongoose.connect(process.env.MONGO_URL, { useUnifiedTopology: true, useNewUrlParser: true });
manager.setEmailDetails(process.env.SENDGRID_API_KEY);

app.get('/', (req, res) => {
    return res.json({success: true, message: "REST api server is up!"});
});

app.get('/api/account/login', async (req, res) => {
    if (req.query.email == undefined || req.query.password == undefined) {
        return res.json({success: false, errors: ['invalid-request']});
    }

    const { email, password } = req.query;
    const result = await manager.verifyLoginDetails(email, password);
    
    return res.json({success: result, errors: []});
});

app.post('/api/account/register', async (req, res) => {
    const { country, firstName, lastName, email, password, confirmPassword, address, city, state, zip, mobile } = req.body;
    console.log(req.body);
    
    //Server-side validation (Passwords)
    if (password != confirmPassword){
        return res.json({
            success: false,
            errors: ['password']
        });
    }

    const searchResult = await manager.accountExists(email);
    if (searchResult) {
        return res.json({
            success: false,
            errors: ['email-exists']
        });
    }

    const creationResult = await manager.registerAccount(country, firstName, lastName, email, password, address, city, state, zip, mobile);
    if (creationResult.length == 0) {
        return res.json({
            success: true,
            message: `Account has been created!`,
            errors: []
        });
    }

    return res.json({
        success: false,
        message: `Failed to create account`,
        errors: creationResult
    });
});

//Web Listener
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});